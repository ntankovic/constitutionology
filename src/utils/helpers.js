import * as d3 from 'd3';

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

let negative_scale = d3.scalePow().exponent(4).domain([-0.2, 0]).range([0, 1]).clamp(true)
let positive_scale = d3.scalePow().exponent(1/4.).domain([1, 1.2]).range([0, 1]).clamp(true)

let scroll_size = 100

function clamp(num, min, max) {
  return num <= min ? min : num >= max ? max : num;
}

function handle_fade(svg, progress) {

    if (progress < 0 || progress > 1) {
        let padding = scroll_size * (1 - negative_scale(progress)) - scroll_size * (positive_scale(progress))
        //svg.style("margin-top", padding.toFixed(0) + "px");
        // svg.attr("transform", `translate(0 ${padding.toFixed(0)})`)
        svg.style("transform", `translate(0, ${padding.toFixed(0)}px)`)

        let svg_progress = progress
        if (svg_progress < 0) {
            svg_progress -= 1;
        }

        svg_progress = 1 - 5*(Math.abs(svg_progress) - 1)
        svg.style("opacity", svg_progress.toFixed(1))


        if (svg_progress < 0.02) {
            svg.style("display", "none");
        }
        else {
            svg.style("display", "");
        }
    }
    else {
        svg.style("opacity", 1);
        svg.style("display", "");
        // svg.style("margin-top", 0);
        // svg.attr("transform", "translate(0 0)")
        svg.style("transform", `translate(0, 0)`)
    }
}

function cp(o) {
    return JSON.parse(JSON.stringify(o));
}

export { handle_fade, cp, clamp }